# SecComp

Study on Secretins of the Competence pilus.

[Weaver, S. J., Sazinsky, M. H., Dalia, T. N., Dalia, A. B. & Jensen, G. J. CryoEM structure of the Vibrio cholerae Type IV competence pilus secretin PilQ. bioRxiv 2020.03.03.975896 (2020) doi:10.1101/2020.03.03.975896.](https://www.biorxiv.org/content/10.1101/2020.03.03.975896v1)


# How to reproduce the data

The following instructions can be run on a Unix based system. There is no fundamental reason why these scripts wouldn't work on Windows but the instructions would be slightly different. Alternatively, Windows users can run Linux systems in virtual machines.

In addition, we expect the system to have the following software installed:

## Third party software requirement

* git
* tar
* bzip2

* node 12.13
* Protein-Protein BLAST 2.7.1+
* MAFFT v7.305b
* HMMER 3.1b2
* RAxML v8.2
* TreeCollapseCL4
* FigTree
* GeneHood cli 0.2.8-0
* @dynocluster/generator 0.1.0

## Clone the repository and install scripts

``` bash
git clone https://gitlab.com/lab-notebook/seccomp.git
cd seccomp
npm install
```

## Preparation


### Step 0: Prep files and directories

``` bash
npm run prep
```

### Step 1: Generate fasta file

First we start by parsing the gene information and making a fasta file with all secretins.

```bash
npm run makeFasta
```

Total of 198900 genes and 41795 of them are unique.

The identifier of the sequence we are interested in is: `GCF_000006745.1-VC2630`


### Step 2: Press HMM database from individual models

Then downloaded the [Secretin profiles](https://gitlab.pasteur.fr/rdenise/diversification-of-tff-sf-data/-/tree/master/profiles_TFF-SF_final) from the Rocha Lab and press them into a single hmm database.

We already have them on `assets` so we can just run:

```bash
cat assets/profiles_TFF-SF_final* > data/secretinfam
hmmpress data/secretinfam
```

> Alternatively, we can get them from the assets.

### Step 3: Use hmmscan to classify each Secretin

```bash
hmmscan --noali --tblout data/secretins.hmmer.tbl data/secretinfam data/secretins.fa > data/secretins.hmmer.log
```

> This may take a while.

### Step 4: Parse HMMER output

The parser assumes that the results are ordered showing the first best hit first for each sequence as they come in the fasta file.

> caveat: If this is not the case, then we need to change src/parseHmm.ts

> Edit: We add a conservative threshold of 1e-40 because the hmm for T3SS is missing and some T3SS secretins were picked up by these models

```bash
npm run parseHmm
```

At this step we found 56941 genes.

### Step 5: Filter the T4P Secretins from fasta file

```bash
npm run filterFasta
```

### Step 6: Trim sequences by Pfam model

We trimmed the boundaries of the Secretin using the envelop coordinates determined by matches to the Secretin model in the Pfam database using HMMER.

```bash
hmmsearch --noali assets/Secretin.hmm data/secretins.t4ap.fa > data/secretins.t4ap.hmmer.log

npm run trimHmm
```

### Step 7: Reduce redundacy with cd-hit

```bash
cd-hit -i data/secretins.t4ap.trimHmm.fa -o data/secretins.t4ap.trimHmm.cd-hit.65.fa -c 0.65 -d 0
```

At this step we have 386 sequences with 65% identity.

Other cutoffs:

|threshold| #seq |
|:-:|:-:|
| 65 | 386 |
| 70 | 558 |
| 80 | 977 |
| 90 | 1726 |
| 100 | 4924 |

We will proceed to lower cutoffs.

### Step 8 Dyno-cluster (version 0.1)

We use DynoCluster to explore the similarity relationships among the sequences. Dyno-cluster uses graph theory concept to group similar sequences that belong to the same component. Here, each sequence is a vertice and vertices are linked if their best BLAST hit is above a threshold.

To install DynoCluster:

``` bash
npm install -g @dynocluster/generator @dynocluster/viewer
```

We will run the generator with the full sequences of `secretins.t4ap.trimHmm.fl.cd-hit.65.fa`. To generate this file we run the `fullLengthScript`:

```bash
npm run buildFullLength
```

and then:

``` bash
dynocluster-app data/secretins.t4ap.trimHmm.fl.cd-hit.65.fa
mv dc.* data/
```

The DynoCluster built the dataset `dc.00fb0540d9.json.gz`.

Using a cutoff of `1E-110` we selected the 1st group with `203` sequences. This was the maximum threshold that included the list of secretins from all the model organisms we wanted to study.

We created the file `secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.lst` in `data` and pasted the list to it.

### Step 8: Extract the protein sequences

``` bash
npm run buildFastaFromList
```

This builds `secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.fa`
Since these are full sequence we took the `trim` tag from the header.

### Step 9: Align sequences

We will use again the L-INS-I:

> Notice that valute passed in `--thread` should be a number close to the number of processor nodes we have in our computer.

``` bash
linsi --thread 24 data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.fa > data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.fa
```

We used Jalview to manually inspect the alignment.

We removed some sequences that were single handed opening large gaps in the alignment and ran linsi again:

```
>Th.niv-GCF_000260135.1-THINI_RS06935-t4ap
>Mo.osl-GCF_001553955.1-AXE82_RS09615-t4ap
>Pl.pac-GCF_000170895.1-PPSIR1_RS44755-t4ap
>Ha.och-GCF_000024805.1-HOCH_RS34430-t4ap
>Be.bac-GCF_000496475.1-MOLA814_RS11740-t4ap
>De.sp.-GCF_001611275.1-DBW_RS07130-t4ap
```

The final alignment has `197` sequences at: `data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.fa`

We also re-run the linsi alignment, just in case:

``` bash
linsi --thread 4 data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.fa > data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.fa
```

This is the final alignment in Supplementary Dataset 1

### Step 10: Phylogeny

We used the same strategy to build a inference with RAxML

> Notice that valute passed in `-T` should be a number close to the number of processor nodes we have in our computer.

``` bash
raxmlHPC-PTHREADS-AVX -T 40 -m PROTGAMMAILG -p 1234555 -x 9876545 -f a -N 200 -s secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.fa -n secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.raxml.protGILG.fa.nwk
```

We collapse the tree at 50% bootstrap values.

``` bash
java -jar ~/Downloads/TreeCollapseCL4.jar -b 50 -f RAxML_bipartitions.secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.raxml.protGILG.fa.nwk
```

This will generate the file `RAxML_bipartitions_50coll.secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.raxml.protGILG.fa.nwk`

Finally used figtree to examine the tree, re-order nodes and root a mid-point and saved the file as `RAxML_bipartitions_50coll.secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.raxml.protGILG.fa.figtree.ord.rmp.nwk`

This is the tree used to order the `197` sequences in the alignment. We used [TREND](http://trend.zhulinlab.org/) for that but we could use a 

The final, reordered alignment is `secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.TREND.ord.raxml.50bsCollapsed.fa`

Both are found in assets.

### Step 11: CD-VIST

We submitted the ordered alignment without gaps `secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.TREND.ord.raxml.50bsCollapsed.nogap.fa` (also in the assets - alternatively, we could save it from Jalview) to [CD-VIST](http://cdvist.zhulinlab.org/) with all default options but skipping RPSBLAST.

We downloaded the result from CD-VIST `data/cd-vist.json`. If you don't want to run CD-VIST, the result is on `assets`. Copy the file to `data` to keep going.

And we ran a counter for significant hits of the AMIN domain:

``` bash
$ npm run cdvistReport

0 AMIN: 27 (13.7%)
1 AMIN: 19 (9.6%)
2 AMIN: 141 (71.6%)
3 AMIN: 10 (5.1%)
```

### Step 11: GeneHood 

We used a script to build a list of stable ids from the sequences.

``` bash
npm run genGeneHoodList
```

We ran GeneHood with 5 genes up and down stream of the Secretin and the collapsed phylogeny.

To install GeneHood cli:

``` bash
npm install -g genehood-cli@latest
```

To generate the dataset:

``` bash
cd data
genehood t4ap --action init
genehood t4ap --addStableIds secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.lst --addRange 5 5
genehood t4ap --action run
```

> This might take a while, if it stops for any reason, try `genehood t4ap --action keepGoing`, if it does not work, please file a bug with GeneHood team.

This will generate a dataset that can be loaded using the [GeneHood webapp](https://next.genehood.io)

If everything worked out so far, the dataset will have `b6344ad86ff2` as the identifying hash (top right corner of the project viewer). This is a unique identifier for the input data. If your project has the same hash (and it should) it means that, at minimum, the input data generated in this pipeline is the same as the one published.

This dataset does not have a phylogenetic tree associated with it yet. We need to parse the final tree so GeneHood can understand it:

> Make sure `RAxML_bipartitions_50coll.secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.raxml.protGILG.fa.figtree.ord.rmp.nwk` is in `data`


``` bash
npm run tree4gn
```

The tree will be saved as `data/tree.genehood.nwk`. Load it on the project using `set newick`. This might take a few seconds.

We included a version of this project with the family groups already identified in the `assets` with the name: `assets/t4ap-2.geneHood.pack.1593759483253.json.gz`

This was our final version of the project analysis and can be used to verify the thresholds used for each family as well as more information of each gene.


### Step 12: Combining the figure

We export the figures generated by GeneHood and CD-Vist and combined them using Inkscape and Adobe Illustrator to make Figure 4 and Supplementary Dataset 1.

