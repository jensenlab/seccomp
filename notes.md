# Notes

The sequence we are interested in is: `GCF_000006745.1-VC2630`

## SQL queries to MiST3

To connect with local MiST3 we attach to the container and run `psql $DATABASE_URL`

Let's select the seqdepot info of ALL Secretins:

```sql
\COPY (SELECT jsonb_agg(row) FROM seqdepot.aseqs row where pfam31->0 is not null and pfam31 @> '[["Secretin"]]') to './Secretins.aseqs.json'
```

but, to get the Secretin gene within Proteobacteria, it is a bit harder:

```sql
\COPY (SELECT jsonb_agg(row) FROM (SELECT g.*, (genomes.id, genomes.accession, genomes.name) as genome FROM (SELECT genes.* FROM genes INNER JOIN seqdepot.aseqs ON seqdepot.aseqs.id = genes.aseq_id where pfam31->0 is not null and pfam31 @> '[["Secretin"]]') g INNER JOIN components ON components.id = g.component_id INNER JOIN genomes ON genomes.id = components.genome_id WHERE genomes.phylum = 'Proteobacteria') row) to 'Secretins.genes.json';
```
## Data processing.

## Third party software requirement

mcl 14-137
Protein-Protein BLAST 2.7.1+
MAFFT v7.305b
HMMER 3.1b2
GeneHood 0.2.8-0


### Step 1: Generate fasta file

First we start by parsing the gene information and making a fasta file with all secretins.

```bash
npm run makeFasta
```

Total of 198900 sequences.

### Step 2: Press HMM database from individual models

Then we downloaded the [Secretin profiles](https://gitlab.pasteur.fr/rdenise/diversification-of-tff-sf-data/-/tree/master/profiles_TFF-SF_final) from the Rocha Lab and press them into a single hmm database:

```bash
hmmpress profiles_TFF-SF_final* > secretinfam
```

### Step 3: Use hmmscan to classify each Secretin

```bash
hmmscan --noali --tblout secretins.hmmer.tbl secretinsfam secretins.fa > secretins.hmmer.log
```

### Step 4: Parse HMMER output

The parser assumes that the results are ordered showing the first best hit first for each sequence as they come in the fasta file.

> caveat: If this is not the case, then we need to change src/parseHmm.ts

> Edit: We add a conservative threshold of 1e-40 because the hmm for T3SS is missing and some T3SS secretins were picked up by this model

```bash
npm run parseHmm
```

At this step we found ~~68434~~ 56941 genes.

### Step 5: Filter the T4P Secretins from fasta file

```bash
npm run filterFasta
```

### Step 6: Trim sequences by Pfam model

We trimmed the boundaries of the Secretin using the envelop coordinates determined by matches to the Secretin model in the Pfam database using HMMER.

```bash
hmmsearch --noali Secretin.hmm secretins.t4ap.fa > secretins.t4ap.hmmer.log

npm run trimHmm
```

### Step 7: Reduce redundacy with cd-hit

```bash
cd-hit -i secretins.t4ap.trimHmm.fa -o secretins.t4ap.trimHmm.cd-hit.65.fa -c 0.65 -d 0
```

At this step we have 558 sequences with 65% identity.

Other cutoffs:

|threshold| #seq |
|:-:|:-:|
| 65 | 386 |
| 70 | 558 |
| 80 | 977 |
| 90 | 1726 |
| 100 | 4924 |

We will proceed to lower cutoffs.

### Step 7a: DynoCluster (failed)

We used dynocluster in the 65% cutoff dataset. Too big. I think there might be better ways to do this. Halted development for the sake of delivering some results.


### Step 7b: Jalview (failed)

Trying to use MAFFT and Jalview to reduce redundancy. Mafft does a poor job aligning the sequence (as expected because of the diversity in the C-terminal domain) and Jalview reports a way high redundancy than it actually exists.

### Step 7c: TREND

We used TREND to get the domain architecture and the sequence similarity relationship of the remainder Secretins.

http://trend.zhulinlab.org/domains/tree/2983?pipeline=full&reduce=false&features=true&eon=3dwr1w8hxup

We used figtree to reorg the tree and root it at midpoint.
Then we re-upload it to partial pipeline on TREND to remake the tree with domain arch.



### Step 7c.1: L-INS-I + RAxML

Instead of using FastTree from trend we use RAxML.

We built the alignment using L-INS-I and the RAxML to build the inference.

``` bash
linsi --thread 12 secretins.t4ap.trimHmm.cd-hit.70.fa > secretins.t4ap.trimHmm.cd-hit.70.linsi.fa
```

``` bash
raxmlHPC-PTHREADS-AVX -T 40 -m PROTGAMMAILG -p 1234555 -x 9876545 -f a -N 200 -s secretins.t4ap.trimHmm.cd-hit.70.linsi.fa -n secretins.t4ap.trimHmm.cd-hit.70.linsi.raxml.protGILG.fa.nwk
```

### Step 7d: Generate files to analytical tools

First we select the full legth sequences from the final dataset.

```
npm run keepFromRef
```

Next, we extract the MiST3 ids from the file to run GeneHood.
```
npm run genGeneHoodList
```

### Step 8: Making the figure

The full image is in Figure S1. To make a more digestable image, we used figtree to make a radial tree.

## Trying this again - 03/07/2020

### Considerations

In the final previous analysis raised two problems:

#### 1 - Secretin domain architecture is not as diverse as defined by the Rocha Lab

The domain architecture of the Secretin selected at the final filtering steps does not seem to be that much different from each other. While this is a finding on its own for the paper, it rise the question if we should be including a longer part of the sequence.

The T4aP HMM model likely select for those anyways.

So, should we align the full sequence or find a better way to select t4ap homologs?

As of right now, there is no better way. We can either pick some random examples or use the Rocha Lab HMM.

#### 2 - (?)


### Solution

Let's see if it is doable to align these sequences altogether now that they are all t4ap.

We start from [Step 5](#step-5-filter-the-t4p-secretins-from-fasta-file).

#### Step 6: decreasing redundancy of the dataset.

```bash
cd-hit -i secretins.t4ap.fa -o secretins.t4ap.cd-hit.65.fa -c 0.65 -d 0
```
|threshold| #seq |
|:-:|:-:|
| 65 | 951 |
| 70 | 1161 |
| 80 | 1856 |
| 90 | 2780 |
| 100 | 10112 |

Let's proceed with the 65% cuttoff.

Next we will force L-INS-I to make the alignment.

#### Step 7a: L-INS-I brute force

``` bash
time linsi --thread 24 secretins.t4ap.cd-hit.65.fa > secretins.t4ap.cd-hit.65.linsi.fa
```
It did not work... it is too slow.

#### Step 7b: BLAST + MCL

We will start by building the clustering using the set with less than 65% identity.

The MCL takes a particular input called `abc`. The `abc` file can be built from the regular output format 6 from BLAST.

``` bash
blastp -query secretins.t4ap.cd-hit.65.fa -db secretins.t4ap.cd-hit.65.db -outfmt 6 -out secretins.t4ap.cd-hit.65.blastp.outfmt6.dat
```

Next we format it to `abc`:

``` bash
cut -f 1,2,11 secretins.t4ap.cd-hit.65.blastp.outfmt6.dat > secretins.t4ap.cd-hit.65.blastp.outfmt6.abc
```

because we did not add a e-value threshold in the BLAST step, we need to clean this up with `awk`

``` bash
awk '$3 <= 1E-30 { printf "%s\n", $0 ; }' secretins.t4ap.cd-hit.65.blastp.outfmt6.abc > secretins.t4ap.cd-hit.65.blastp.outfmt6.1E-30.abc
```


Now we will generate the `mci` and `tab` files.

``` bash
mcxload -abc secretins.t4ap.cd-hit.65.blastp.outfmt6.1E-30.abc --stream-mirror --stream-neg-log10 -stream-tf 'ceil(200)' -o secretins.t4ap.cd-hit.65.blastp.outfmt6.1E-30.mci -write-tab secretins.t4ap.cd-hit.65.blastp.outfmt6.1E-30.tab
```

Finally, we can run the MCL:

``` bash
mcl secretins.t4ap.cd-hit.65.blastp.outfmt6.1E-30.mci -I 6 -use-tab secretins.t4ap.cd-hit.65.blastp.outfmt6.1E-30.tab -o secretins.t4ap.cd-hit.65.blastp.outfmt6.1E-30.mcl.I6.clst
```

With `1E-30` we get the following number of clusters:

|cluster | # of sequences |
|:|:|
|1 |701|
|2 |205|
|3 | 25|
|4 |  9|
|5 |  7|
|6 |  3|
|7 |  1|

With `1E-40` we get the following number of clusters:

|cluster | # of sequences |
|:|:|
|1 |701|
|2 |205|
|3 | 25|
|4 |  9|
|5 |  7|
|6 |  3|
|7 |  1|



### Step 8: split into fasta files

``` bash
npm run splitFasta
```

### Step 9: cd-hit again in each file.

File 1:

``` bash
for i in `ls secretins.t4ap.cd-hit.65.blastp.mcl.split.*.fa`; do cd-hit -i ${i} -o ${i/\.fa/\.cd-hit.65.fa} -c 0.65; done
```

Didn't work... cant easily reduce the size of the system there.

## Third time is the charm

The major problem here is that we need to take too many turns to not report the obvious: The Rocha Lab secretin HMM is biased towards finding secretins that are working together with a known system.

So I developed dynocluster a little further.

Let's try it again from the [Step 7a](#step-7a-dynocluster-failed).

### Step 7a (redo): Dyno-cluster (n-matrix branch)

We will run the generator with the full sequences of `secretins.t4ap.trimHmm.fl.cd-hit.65.fa`

The DynoCluster built the dataset `dc.kc9bo0in.json.gz`.

Using a cutoff of 1-E110 we selected the 1st group with 203 sequences. This was the maximum threshold that included the model organism list we wanted to compare with.

We copied a list of the tag of the sequence to: `secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.lst`

### Step 8: Extract the protein sequences.

We modified the buildFullLength to get the sequences.

```
npm run buildFastaFromList.ts
```

This builds `secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.fa`
Since these are full sequence we took the `trim` tag from the header.

### Step 9: Align sequences

We will use again the L-INS-I:
```
linsi --thread 24 secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.fa > secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.fa
```

We used Jalview to manually inspect the alignment.

We removed some sequences that were single handed opening large gaps in the alignment and ran linsi again.

The final alignment has 197 sequences at: 

### Step 10: Phylogeny

We used the same strategy to build a inference with RAxML

``` bash
raxmlHPC-PTHREADS-AVX -T 40 -m PROTGAMMAILG -p 1234555 -x 9876545 -f a -N 200 -s secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.fa -n secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.raxml.protGILG.fa.nwk
```

We collapse the tree at 50% bootstrap values.

``` bash
java -jar ~/Downloads/TreeCollapseCL4.jar -b 50 -f RAxML_bipartitions.secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.raxml.protGILG.fa.nwk
```

Finally used figtree to examine the tree, re-order nodes and root a mid-point.

### Step 11: CD-VIST

We submitted the ordered alignment without gaps`
secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.TREND.ord.raxml.50bsCollapsed.nogap.fa` to CD-VIST with all default options but skipping RPSBLAST.

We downloaded the result from CD-VIST `raw/cd-vist.json`.

And we ran a counter for significant hits of the AMIN domain:
```
$ npm run cdvistReport

0 AMIN: 27 (13.7%)
1 AMIN: 19 (9.6%)
2 AMIN: 141 (71.6%)
3 AMIN: 10 (5.1%)
```

### Step 11: GeneHood 

We used a script to build a list of stable ids from the sequences.

``` bash
npm run genGeneHoodList
```

We ran GeneHood with 5 genes up and down stream of the Secretin and the collapsed phylogeny.

### Step 12: Combining the figure.

We used Inkscape and Adobe Illustrator to combine GeneHood and CD-VIST outputs.

