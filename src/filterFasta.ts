import { BioSeq, FastaParserStream, FastaUtils } from 'bioseq-ts';
import fs from 'fs';
import through2 from 'through2';

const fastaFilename = 'data/secretins.fa';
const inputFilename = 'data/secretins.hmmer.parsed.1e-40.lst';
const outputFilename = 'data/secretins.t4ap.fa';

const t4ap = fs
  .readFileSync(inputFilename)
  .toString()
  .split('\n')
  .filter(i => i !== '');
const fastaStream = fs.createReadStream(fastaFilename);
const outputStream = fs.createWriteStream(outputFilename);

const total = t4ap.length;

const fastaParser = new FastaParserStream();

const parser = () => {
  let counter = 0;
  return through2.obj(
    function(entry: BioSeq, enc: any, next) {
      const index = t4ap.indexOf(entry.header);
      if (index !== -1) {
        counter++;
        if (counter % 500 === 0) {
          console.log(`Found ${counter}/${total}. Remaining: ${t4ap.length}`);
        }
        this.push(`>${entry.header}-t4ap\n${entry.seq}\n`);
        t4ap.splice(index, 1);
      }
      next();
    },
    next => {
      console.log(`number of sequences in fasta: ${counter}`);
      console.log('Not Found: ');
      console.log(t4ap.join('\n'));
      next();
    },
  );
};

fastaStream
  .pipe(fastaParser)
  .pipe(parser())
  .pipe(outputStream);
