import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import { GenomeStream } from "mist3-ts";
import { Transform } from "stream";
import through2 from "through2"

class GenomeToGenes extends Transform {
  public processed: number;
  public success: number;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;
  private readonly perPage: number;
  private readonly genomes: string[];
  private readonly mist3LogLevel: LogLevelDescType;

  public constructor(
    perPage: number = 100,
    logLevel: LogLevelDescType = "info",
    mist3LogLevel: LogLevelDescType = "silent"
  ) {
    super({ objectMode: true });
    this.perPage = perPage;
    this.logLevel = logLevel;
    const logger = new Logger(this.logLevel);
    this.log = logger.getLogger(`GenomeToGenes`);
    this.processed = 0;
    this.success = 0;
    this.genomes = [];
    this.mist3LogLevel = mist3LogLevel;
  }

  public async _transform(chunk: string, enc: any, next: () => void) {
    const genome = chunk;
    this.processed++;
    if (this.genomes.indexOf(genome) === -1) {
      this.genomes.push(genome);
      const mist3GenomeStream = new GenomeStream(
        this.perPage,
        this.mist3LogLevel
      );
      this.log.info(`... waiting on MiST3 to return genes from ${genome}...`);
      await mist3GenomeStream.fetchAllGenes(genome).then(stream => {
        stream.on("data", (d: any) => this.push(d));
        stream.on("end", next);
      });
    }
  }

  private sink() {
    return through2.obj((chunk, enc, next) => {
      this.log.info(chunk);
      next();
    });
  }
}

export { GenomeToGenes };
