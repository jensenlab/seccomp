import { IRegArch } from "regarch/lib/interfaces";

const duf2147: IRegArch = {
  patterns: [
    {
      name: "duf2147",
      npos: [],
      pos: [
        {
          name: "^",
          resource: "ra"
        },
        {
          name: "DUF2147",
          resource: "pfam31"
        },
        {
          name: "$",
          resource: "ra"
        }
      ]
    }
  ]
};

const duf386: IRegArch = {
  patterns: [
    {
      name: "duf386",
      npos: [],
      pos: [
        {
          name: "^",
          resource: "ra"
        },
        {
          name: "DUF386",
          resource: "pfam31"
        },
        {
          name: "$",
          resource: "ra"
        }
      ]
    }
  ]
};

export { duf386, duf2147 };
