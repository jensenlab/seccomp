import fs from "fs";
import { Logger } from "loglevel-colored-prefix";
import through2 from "through2";

import { AppendAseqInfo } from "./AppendAseqInfo";
import { FilterByRegArch } from "./FilterByRegArch";
import { GenomeToGenes } from "./GenomesToGenes";
import { jsonToStream } from "./jsonToStream";
import { duf2147, duf386 } from "./regarchDefinitions";

const logger = new Logger("info");
const log = logger.getLogger("makeFasta");

const geneFile = `raw/Secretins.genes.json`;
const aseqFile = `raw/Secretins.aseqs.json`;
const fileFastaOut = `data/secretins.fa`;

const aseqInfo = JSON.parse(fs.readFileSync(aseqFile).toString()) as Array<{
  id: string;
  sequence: string;
}>;

log.info(`Number of aseqs loaded: ${aseqInfo.length}`);

const geneStream = jsonToStream(geneFile);
let counter = 0;

const sink = () =>
  through2.obj(function(
    chunk: {
      genome: { f3: string };
      stable_id: string;
      aseq_id: string;
      sequence: string;
    },
    enc,
    next
  ) {
    counter++;
    if (counter % 500 === 0) {
      log.info(`Progress: ${counter}`);
    }
    const aseq = aseqInfo.find((d: { id: string }) => d.id === chunk.aseq_id);
    const nameParts = chunk.genome.f3.split(" ");
    let org = "und";
    if (nameParts.length > 1) {
      org = `${nameParts[0].slice(0, 2)}.${nameParts[1].slice(0, 3)}`;
    }
    let seq = null;
    if (aseq) {
      seq = aseq.sequence;
    }
    const entry = `>${org}-${chunk.stable_id}\n${seq}\n`;
    this.push(entry);
    next();
  });

const w = fs.createWriteStream(fileFastaOut);

const c = sink();
geneStream
  .pipe(c)
  .pipe(w)
  .on("error", err => {
    throw err;
  });

