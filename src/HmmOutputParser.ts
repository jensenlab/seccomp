import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Transform } from 'stream';

const POS_START = 12;
const POS_STOP = 13;
const POS_STATUS = 1;

interface Iitem {
  t: string;
  h: string;
  e: number;
}

interface Icounter {
  bad: number;
  processed: number;
  filtered: number;
  unique: number;
}

interface IData {
  header: string;
  start: number;
  stop: number;
  status: string;
}

class HmmOutputParser extends Transform {
  private current: IData;
  private readonly buffer: string[];
  private readonly logger: Logger;
  private readonly counter: Icounter;

  public constructor(loglevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.current = {
      header: '',
      start: -1,
      stop: -1,
      status: '',
    };
    this.buffer = [];
    this.counter = {
      bad: 0,
      filtered: 0,
      processed: 0,
      unique: 0,
    };
    this.logger = new Logger(loglevel);
  }

  public _transform(chunk: string, env: any, next: () => void) {
    const content = (this.buffer.pop() || '') + chunk.toString();
    const lines = this.splitLines(content);
    const items = this.processLines(lines);
    items.forEach(r => this.push(r));
    next();
  }

  public _flush(next: () => void) {
    const log = this.logger.getLogger('HmmOutputParser::flush');
    const content = this.buffer.pop() || '';
    const lines = this.splitLines(content);
    const items = this.processLines(lines);
    items.forEach(r => this.push(r));
    if (this.current.header !== '') {
      this.push(this.current);
    }
    log.info(`Processed  ${this.counter.processed} sequences.`);
    log.info(`Bad seqs  ${this.counter.bad} sequences.`);
    next();
  }

  private processLines(lines: string[]): IData[] {
    const log = this.logger.getLogger('HmmOutputParser::processLines');
    const records: IData[] = [];
    lines.forEach(line => {
      const fields = line.split(' ').filter(l => l !== '');
      if (fields[0] === '>>') {
        this.counter.processed++;
        this.current.header = fields[1];
      } else if (fields[POS_STATUS] === '!' && this.current.header !== '') {
        this.current.start = parseInt(fields[POS_START], 10);
        this.current.stop = parseInt(fields[POS_STOP], 10);
        this.current.status = fields[POS_STATUS];
      } else if (fields[POS_STATUS] === '?') {
        log.warn(this.current);
        log.warn(fields);
        this.counter.bad++;
      } else if (
        fields.length === 0 &&
        this.current.status === '!' &&
        this.current.header !== '' &&
        this.current.start !== -1
      ) {
        // Log.info(this.current)
        records.push(this.current);
        this.current = {
          header: '',
          start: -1,
          stop: -1,
          status: '',
        };
      }
    });
    return records;
  }

  private splitLines(content: string): string[] {
    const log = this.logger.getLogger('HmmOutputParser::parse');
    const lines = content.split('\n');
    if (lines.length > 1) {
      const rest = lines.pop();
      if (rest) {
        this.buffer.push(rest);
      }
    }
    return lines;
  }
}

export { HmmOutputParser };
