import fs from 'fs';

import { HmmTblParser } from './HmmTblParser';

const hit_threshold = 1E-40

const filename = 'data/secretins.hmmer.tbl';
const fileout = `data/secretins.hmmer.parsed.${hit_threshold}.lst`;


const input = fs.createReadStream(filename);
const output = fs.createWriteStream(fileout);

const parser = new HmmTblParser('T4P_pilQ', hit_threshold);

input
  .pipe(parser)
  .pipe(output)
  .on('error', err => {
    throw new Error('ops');
  });
