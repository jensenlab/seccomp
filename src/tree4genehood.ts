import fs from 'fs';
import { Tree } from 'phylogician-ts';

const originalNwkFilename = 'data/RAxML_bipartitions_50coll.secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.raxml.protGILG.fa.figtree.ord.rmp.nwk';
const finalNwkFilename = 'data/tree.genehood.nwk';

const nwk = fs.readFileSync(originalNwkFilename).toString();

const tree = new Tree();

tree.buildTree(nwk.replace(/\'/g, ''));

tree.getAllLeafIds().forEach(id => {
  const node = tree.getNode(id);
  const nameParts = node.name.split('-');
  const newName = `${nameParts[1]}-${nameParts[2]}`;
  console.log(`${node.name} => ${newName}`);
  node.name = newName;
});

const outNwk = tree.writeNewick();

fs.writeFileSync(finalNwkFilename, outNwk);
