import fs from "fs";
// tslint:disable-next-line: no-submodule-imports
import StreamArray from "stream-json/streamers/StreamArray";
import through2 from "through2";

interface IInputObject {
  key: number,
  value: string
}

export const jsonToStream = (sourceFile: string) => {
  const cheaStream = fs.createReadStream(sourceFile);
  const parser = StreamArray.withParser();
  const cleanUp = through2.obj(function(chunk: IInputObject, enc, next){
    this.push(chunk.value)
    next();
  });
  return cheaStream.pipe(parser).pipe(cleanUp);
};