import { BioSeq, FastaParserStream } from 'bioseq-ts';
import fs from 'fs';
import through2 from 'through2';

import { HmmOutputParser } from './HmmOutputParser';

interface IData {
  header: string;
  start: number;
  stop: number;
}

const hmmFilename = 'data/secretins.t4ap.hmmer.log';
const fastaFilename = 'data/secretins.t4ap.fa';
const fileoutTrimmed = 'data/secretins.t4ap.trimHmm.fa';
const fileoutFl = 'data/secretins.t4ap.trimHmm.fl.fa';

const input = fs.createReadStream(hmmFilename);
const outputTrimmed = fs.createWriteStream(fileoutTrimmed);
const outputFl = fs.createWriteStream(fileoutFl);

const fasta = fs.createReadStream(fastaFilename);

const hmmParser = new HmmOutputParser();
const fastaParser = new FastaParserStream();

const problems: BioSeq[] = [];

const addBoundary = (boundaries: IData[]) => {
  let counter = 0;
  return through2.obj(function(entry: BioSeq, enc: any, next: () => void) {
    const boundary = boundaries.find(b => b.header === entry.header);
    if (boundary) {
      const item = {
        boundary,
        entry,
      };
      counter++;
      this.push(item);
    } else {
      problems.push(entry);
    }
    next();
  });
};

const trimFasta = through2.obj(function(chunk: { entry: BioSeq; boundary: IData }, enc: any, next) {
  this.push(
    `>${chunk.entry.header}-trim\n${chunk.entry.seq.substr(
      chunk.boundary.start,
      chunk.boundary.stop - chunk.boundary.start,
    )}\n`,
  );
  next();
});

const fullLenFasta = through2.obj(function(chunk: { entry: BioSeq; boundary: IData }, enc: any, next) {
  this.push(`>${chunk.entry.header}-trim\n${chunk.entry.seq}\n`);
  next();
});

const boundaries: IData[] = [];

input
  .pipe(hmmParser)
  .on('data', data => {
    boundaries.push(data);
  })
  .on('end', () => {
    console.log(boundaries.length);
    const parsed = fasta.pipe(fastaParser).pipe(addBoundary(boundaries));

    parsed.pipe(trimFasta).pipe(outputTrimmed);
    parsed.pipe(fullLenFasta).pipe(outputFl);
  });
