import fs from 'fs';

const OVERLAP_TOLERANCE = 2; // In amino acids

const cdvist = JSON.parse(fs.readFileSync(`data/cd-vist.json`).toString());

const check4overlap = (entries: any[]) => {
  const results = [];
  entries.sort((a, b) => a.start - b.start);
  for (let i = 0; i < entries.length - 1; i++) {
    const matchThis = [];
    for (let j = i + 1; j < entries.length; j++) {
      if (entries[i].end - entries[j].start > OVERLAP_TOLERANCE) {
        matchThis.push(j);
      }
    }
    results.push(matchThis);
  }
  return results;
};

const solveOverlap = (entries: any[]) => {
  const overlaps = check4overlap(entries);
  const resolved = entries.map((e, i) => ({ e, id: i }));
  const popList: number[] = [];
  overlaps.forEach((overlap: number[], i: number) => {
    overlap.forEach((j: number) => {
      popList.push(entries[i].score > entries[j].score ? j : i);
    });
  });
  return resolved.filter(e => !popList.includes(e.id)).map(e => e.e);
};

const countAmins = cdvist.entries.map((item: any) => {
  const name = item.header;
  const amins = item.segments.assigned.filter(
    (e: any) => e.assigned && e.name.match(/AMIN/) && (e.prob === 'NaN' || e.prob > 60),
  );
  const resolved = solveOverlap(amins);

  return { name, count: resolved.length };
});

const report = (): number[] => {
  const counts: number[] = [];
  countAmins.forEach((item: { name: string; count: number }) => {
    if (counts[item.count]) {
      counts[item.count]++;
    } else {
      counts[item.count] = 1;
    }
  });
  return counts;
};

// Console.log(JSON.stringify(report, null, ' '));
console.log(`Reporting number of sequences with different counts of AMIN`);

const r = report();
const totalAmin = r.reduce((a, b) => a + b, 0);

r.forEach((i, j) => {
  console.log(`${j} AMIN: ${i} (${(100*i/totalAmin).toFixed(1)}%)`);
});
