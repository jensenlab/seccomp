import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import { Genome } from "mist3-ts";
import { Transform } from "stream";

class GenomeVersionToGenomeInfo extends Transform {
  public processed: number;
  public success: number;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;
  private readonly bufferSize: number;
  private genomeBuffer: any[];
  private readonly genomes: string[];
  private readonly mist3LogLevel: LogLevelDescType;

  public constructor(
    bufferSize: number = 100,
    logLevel: LogLevelDescType = "info",
    mist3LogLevel: LogLevelDescType = "silent"
  ) {
    super({ objectMode: true });
    this.bufferSize = bufferSize;
    this.logLevel = logLevel;
    const logger = new Logger(this.logLevel);
    this.log = logger.getLogger(`GenomeVersionToGenomeInfo`);
    this.processed = 0;
    this.success = 0;
    this.genomes = [];
    this.genomeBuffer = [];
    this.mist3LogLevel = mist3LogLevel
  }

  public async _transform(chunk: string, enc: any, next: () => void) {
    const genome = chunk;
    this.processed++;
    if (this.genomes.indexOf(genome) === -1) {
      this.genomeBuffer.push(genome);
      this.genomes.push(genome);
    }
    this.log.debug(`number of genomes on buffer: ${this.genomeBuffer.length}`);
    if (this.genomeBuffer.length === this.bufferSize) {
      const mist3Genome = new Genome(this.mist3LogLevel);
      this.log.info(
        `... waiting on MiST3 to return ${this.genomeBuffer.length} genomes ...`
      );
      const genomeInfoList = await mist3Genome.fetchDetailsMany(
        this.genomeBuffer
      );
      this.log.debug(
        `Information for ${this.genomeBuffer.length} genomes received.`
      );
      genomeInfoList.forEach((genome: any) => {
        this.log.info(
          `Pushing genome ${genome.name} from strain ${genome.strain}`
        );
        this.success++;
        this.push(genome);
      });
      this.genomeBuffer = [];
    }
    next();
  }

  public async _flush(next: () => void) {
    if (this.genomeBuffer.length) {
      this.log.info(
        `Getting information of the last ${this.genomeBuffer.length} genomes on MiST3`
      );
      const mist3Genome = new Genome(this.mist3LogLevel);
      this.log.info(
        `... waiting on MiST3 to return ${this.genomeBuffer.length} genomes ...`
      );
      const genomeInfoList = await mist3Genome.fetchDetailsMany(
        this.genomeBuffer
      );
      this.log.debug(
        `Information for ${this.genomeBuffer.length} genomes received.`
      );
      this.log.debug(genomeInfoList)
      genomeInfoList.forEach((genome: any) => {
        this.log.info(
          `Pushing genome ${genome.name} from strain ${genome.strain}`
        );
        this.success++;
        this.push(genome);
      });
    }
    this.log.warn(`Processed: ${this.processed}. Success: ${this.success}`);
    this.genomeBuffer = [];
    next();
  }
}

export { GenomeVersionToGenomeInfo };
