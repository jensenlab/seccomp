import { BioSeq, FastaParserStream, FastaUtils } from 'bioseq-ts';
import fs from 'fs';
import through2 from 'through2';

const filenameRef = 'data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.fa';
const filenameOutput = 'data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.linsi.manualedit.linsi.lst';

const output = fs.createWriteStream(filenameOutput);

const fastaParser = new FastaParserStream();

const parser = () =>
  through2.obj(function(entry: BioSeq, enc: any, next) {
    const headerParts = entry.header.split('-')
    const id = `${headerParts[1]}-${headerParts[2]}`
    this.push(`${id}\n`)
    next();
  });

fs.createReadStream(filenameRef)
  .pipe(fastaParser)
  .pipe(parser())
  .pipe(output);
