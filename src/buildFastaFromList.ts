import { BioSeq, FastaParserStream, FastaUtils } from 'bioseq-ts';
import fs from 'fs';
import through2 from 'through2';

const filenameRef = 'data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.lst';
const filemaneMain = 'data/secretins.t4ap.trimHmm.fl.fa';
const filenameOutput = 'data/secretins.t4ap.trimHmm.fl.cd-hit.65.cluster1.1E-110.fa';

const headers = fs
  .readFileSync(filenameRef)
  .toString()
  .split('\n');

const output = fs.createWriteStream(filenameOutput);

const fastaParser = new FastaParserStream();

const parser = () =>
  through2.obj(function(entry: BioSeq, enc: any, next) {
    if (headers.indexOf(entry.header) !== -1) {
      this.push(`>${entry.header.replace('-trim', '')}\n${entry.seq}\n`);
    }
    next();
  });

fs.createReadStream(filemaneMain)
  .pipe(fastaParser)
  .pipe(parser())
  .pipe(output);
