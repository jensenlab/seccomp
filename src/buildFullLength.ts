import { BioSeq, FastaParserStream, FastaUtils } from 'bioseq-ts';
import fs from 'fs';
import through2 from 'through2';

const filenameRef = 'data/secretins.t4ap.trimHmm.cd-hit.65.fa';
const filemaneMain = 'data/secretins.t4ap.trimHmm.fl.fa';
const filenameOutput = 'data/secretins.t4ap.trimHmm.fl.cd-hit.65.fa';

const fu = new FastaUtils();
const refs = fu.parse(fs.readFileSync(filenameRef).toString());
const headers = refs.getBioSeqs().map(e => e.header);

const output = fs.createWriteStream(filenameOutput);

const fastaParser = new FastaParserStream();

const parser = () =>
  through2.obj(function(entry: BioSeq, enc: any, next) {
    if (headers.indexOf(entry.header) !== -1) {
      this.push(`>${entry.header}\n${entry.seq}\n`);
    }
    next();
  });

fs.createReadStream(filemaneMain)
  .pipe(fastaParser)
  .pipe(parser())
  .pipe(output);
