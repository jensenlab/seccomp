import { BioSeqSet, FastaUtils } from 'bioseq-ts/lib';
import fs from 'fs';

const clusterFilename = `data/secretins.t4ap.cd-hit.65.blastp.outfmt6.1E-80.mcl.I6.clst`;
const fastaFilename = `data/secretins.t4ap.cd-hit.65.fa`;
const outputFilenamePrefix = `data/secretins.t4ap.cd-hit.65.blastp.1E-80.mcl.split`;

const fu = new FastaUtils();
const bioseqs = fu.parse(fs.readFileSync(fastaFilename).toString());

fs.readFileSync(clusterFilename)
  .toString()
  .split('\n')
  .filter(clusterString => clusterString !== '')
  .map((cluster, i) => {
    const headers = new Set(cluster.split('\t'));
    const subset = bioseqs.getBioSeqs().filter(entry => headers.has(entry.header));
    const newSet = new BioSeqSet(subset);
    fs.writeFileSync(`${outputFilenamePrefix}.${i + 1}.fa`, fu.write(newSet));
  });
