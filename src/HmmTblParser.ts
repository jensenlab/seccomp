import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Transform } from 'stream';

const POS_EVALUE = 4;
const POS_TYPE = 0;
const POS_HEADER = 2;

interface Iitem {
  t: string;
  h: string;
  e: number;
}

interface Icounter {
  processed: number;
  filtered: number;
  unique: number;
}

class HmmTblParser extends Transform {
  private readonly type: string;
  private readonly threshold: number;
  private current: string;
  private readonly buffer: string[];
  private readonly logger: Logger;
  private readonly counter: Icounter;

  public constructor(type: string, threshold: number = 1, loglevel: LogLevelDescType = 'info') {
    super();
    this.type = type;
    this.threshold = threshold;
    this.current = '';
    this.buffer = [];
    this.counter = {
      filtered: 0,
      processed: 0,
      unique: 0,
    };
    this.logger = new Logger(loglevel);
  }

  public _transform(chunk: string, env: any, next: () => void) {
    const log = this.logger.getLogger('HmmTblParser::transform');
    const content = (this.buffer.pop() || '') + chunk.toString();
    const lines = this.splitLines(content);
    lines.forEach(line => {
      const item = this.parseSingleLine(line);
      if (item.h !== this.current && item.e < this.threshold) {
        this.current = item.h;
        this.counter.unique++;
        if (item.t === this.type) {
          this.counter.filtered++;
          this.push(`${item.h}\n`);
        }
      }
    });
    next();
  }

  public _flush(next: () => void) {
    const log = this.logger.getLogger('HmmTblParser::flush');
    const line = this.buffer.pop();
    if (line) {
      const item = this.parseSingleLine(line);
      if (item.h !== this.current) {
        this.current = item.h;
        if (item.t === this.type) {
          log.info(`One last sequence... ${item.h}`)
          this.counter.filtered++;
          this.push(`${item.h}\n`);
        }
      }
    }
    log.info(`Filtered ${this.counter.filtered}/${this.counter.unique} sequences as ${this.type}.`);          
    next();
  }

  private splitLines(content: string): string[] {
    const log = this.logger.getLogger('HmmTblParser::parse');
    const lines = content.split('\n');
    if (lines.length > 1) {
      const rest = lines.pop();
      if (rest) {
        this.buffer.push(rest);
      }
    }
    this.counter.processed += lines.length;
    log.info(`Processing ${lines.length} to a total of ${this.counter.processed}.`);
    return lines;
  }

  private parseSingleLine(line: string): Iitem {
    const fields = line.split(' ').filter(l => l !== '');
    const obj: Iitem = {
      e: parseFloat(fields[POS_EVALUE]),
      h: fields[POS_HEADER],
      t: fields[POS_TYPE],
    };
    return obj;
  }
}

export { HmmTblParser };
