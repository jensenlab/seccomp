import fs from 'fs';
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import through2 from 'through2';

const loglevel: LogLevelDescType = 'info';
const logger = new Logger(loglevel);

const filename = 'data/secretins.hmmer.tbl';
const fileout = 'data/secretins.hmmer.parsed.json';

const input = fs.createReadStream(filename);
const output = fs.createWriteStream(fileout);

interface Iitem {
  t: string;
  s: string;
  e: number;
}

const parseLine = (line: string): Iitem => {
  const fields = line.split(' ').filter(l => l !== '');
  return {
    t: fields[0],
    s: fields[2],
    e: parseFloat(fields[4]),
  };
};

const parse = () => {
  const buffer: string[] = [];
  let counter = 0;
  const log = logger.getLogger('parseHmm::parse');
  return through2.obj(
    function(chunk: Buffer, enc: any, next) {
      const content = (buffer.pop() || '') + chunk.toString();
      const lines = content.split('\n');
      if (lines.length > 0) {
        const rest = lines.pop();
        if (rest) {
          buffer.push(rest);
        }
      }
      log.info(`Adding ${lines.length} to ${counter} processed lines.`);
      counter += lines.length;
      lines.filter(l => l[0] !== '#').forEach(line => this.push(parseLine(line)));
      if (lines.length === 1) {
        console.log(lines)
        throw new Error('hey')
      }
      next();
    },
    function(next) {
      const line = buffer.pop();
      if (line) {
        counter++;
        log.info(`Processed a total of ${counter} lines`);
        this.push(parseLine(line));
      }
      next();
    },
  );
};

const filter = (type: string) => {
  const set: Iitem[] = [];
  let counter = 0;
  return through2.obj(
    function(chunk: Iitem, enc: any, next) {
      const current = set.find(d => d.s === chunk.s);
      counter++;
      if (!current) {
        set.push(chunk);
      } else if (current.e > chunk.e) {
        current.e = chunk.e;
        current.t = chunk.t;
      }
      next();
    },
    function(next) {
      console.log(counter);
      this.push(
        JSON.stringify(
          set.filter(i => i.t === type).map(i => i.s),
          null,
          ' ',
        ),
      );
      next();
    },
  );
};

input
  .pipe(parse())
  .pipe(filter('T4P_pilQ'))
  .pipe(output)
  .on('error', err => {
    throw new Error('ops');
  });
